#!/bin/bash

########   RESEAU LIBRE FOR UBUNTU TOUCH   #########
## row, row, fight the powa
## 

##To Do: 
#check for required software
#check for root
#check for ipv6 compatibility
#set some more variables and shit
#run with IP commands instead
#exit on check fails
#if IPv6, then run ipv6 only
#make interactive option (asks user preferences)
#clean up code with some fuckin functions and variables

NODE_NAME=relais.reseaulibre.ca

NETDEVICES=( `iw dev | awk '$1=="Interface"{print $2}'` )
INTERFACE=${NETDEVICES[0]} #literally just picks the first one.  need to find a better way to do this.
in=$INTERFACE
MODMAC=$(awk -F: '{OFS=""} {print $1,$2,":",$3,$4,":",$5,$6}' /sys/class/net/$INTERFACE/address)

if [[ "$OSTYPE" == "linux-gnu"* ]]; then #probably has a full OS, on a desktop or laptop or something

## Launch a sleeping child process that will be "waited" next
sleep infinity & PID=$!
## Trap "graceful" kills and use them to kill the sleeping child
trap "kill $PID" TERM
## Launch a subprocess not attached to this script that will trigger
## commands after its end
( sh -c "
    ## Watch main script PID. Sleep duration can be ajusted 
    ## depending on reaction speed you want
    while [ -e /proc/$$ ]; do sleep 3; done
    ## Kill the infinite sleep if it's still running
    [ -e /proc/$PID ] && kill $PID
    ## Commands to launch after any stop
    echo "Terminating"
    service network-manager start
" & )    

service network-manager stop

iwconfig $in mode ad-hoc essid $NODE_NAME ap 02:CA:FF:EE:BA:BE channel 1
ifconfig $in up 172.16.1.101/32 netmask 255.240.0.0
##ifconfig $in up 172.16.1.255/32 netmask 255.255.255.255
##ifconfig $in up netmask 255.240.0.0
ifconfig $in inet6 add fd64:2c08:9fa7:face:1e55:$MODMAC/64
libertine-launch --id xenial babeld -d1 $in


elif [[ "$OSTYPE" == "linux-gnueabihf"* ]]; then #using ubuntu-touch... probably....maybe.

## Launch a sleeping child process that will be "waited" next
sleep infinity & PID=$!
## Trap "graceful" kills and use them to kill the sleeping child
trap "kill $PID" TERM
## Launch a subprocess not attached to this script that will trigger
## commands after its end
( sh -c "
    ## Watch main script PID. Sleep duration can be ajusted 
    ## depending on reaction speed you want
    while [ -e /proc/$$ ]; do sleep 3; done
    ## Kill the infinite sleep if it's still running
    [ -e /proc/$PID ] && kill $PID
    ## Commands to launch after any stop
    echo "Terminating"
    service network-manager start
" & )    

service network-manager stop

iwconfig $in mode ad-hoc essid $NODE_NAME ap 02:CA:FF:EE:BA:BE channel 1
ifconfig $in up 172.16.1.101/32 netmask 255.240.0.0
##ifconfig $in up 172.16.1.255/32 netmask 255.255.255.255
##ifconfig $in up netmask 255.240.0.0
ifconfig $in inet6 add fd64:2c08:9fa7:face:1e55:$MODMAC/64
libertine-launch --id xenial babeld -d1 $in


elif [ -n `cat /etc/os-release | grep OpenWrt`]; then
    echo "OpenWrt support comming soon"

else
    echo "Cannot determine platform"
    exit
fi



