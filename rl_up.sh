# For phone or something. No IPv4.
NODE_NAME=relais.reseaulibre.ca

INTERFACE=wlp2s0
MAC=$(awk -F: '{OFS=""} {print $1,$2,":",$3,$4,":",$5,$6}' /sys/class/net/$INTERFACE/address)
echo $MAC

exit;

systemctl stop wpa_supplicant
ifconfig $INTERFACE down
iwconfig $INTERFACE mode ad-hoc essid $NODE_NAME ap 02:CA:FF:EE:BA:BE channel 1
ifconfig $INTERFACE up netmask 255.255.255.255
ifconfig $INTERFACE inet6 add fd64:2c08:9fa7:face:1e55:$MAC/64
